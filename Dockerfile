FROM golang:alpine
LABEL maintaner="Aroop Roelofs <me@finlaydag33k.nl>"
RUN apk update && \
  apk add --no-cache git && \
  mkdir /build && \
  cd /build && \
  git clone https://github.com/chihaya/chihaya.git . && \
  go build ./cmd/chihaya

FROM golang:alpine
WORKDIR /app
COPY --from=0 /build/chihaya /app/chihaya
ADD config.example.yaml /app/config.yaml

CMD /app/chihaya --config config.yaml